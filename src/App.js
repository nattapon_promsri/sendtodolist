import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom'
import UserPage from './pages/UserPage';
import ToDoPage from './pages/ToDoPage';

function App() {
  return (
    <BrowserRouter>
      <Route path="/userPage" component={UserPage} exact={true} />
      <Route path="/userPage/:user_id/todo" component={ToDoPage} />
    </BrowserRouter>
  );
}

export default App;

{/* <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div></div> */}