import React, { useState, useEffect } from 'react'
import { List, Typography, Descriptions, Select, Button } from 'antd';



const ToDoPage = (Props) => {

    const [user, setUser] = useState()
    const [todoList, setTodoList] = useState([])

    const [selectDoneDoing, setSelectDoneOrDoing] = useState('all')
    const { Option } = Select;

    useEffect(() => {
        fetchUserData();
        fetchToDoData();
    }, [])

    const fetchUserData = () => {
        const userId = Props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(Response => Response.json())
            .then(data => {
                setUser(data)
            })

            .catch(error => console.log(error))
    }

    const fetchToDoData = () => {
        const userId = Props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(Response => Response.json())
            .then(data => {
                setTodoList(data)
                // genSelected(data)
            })
            .catch(error => console.log(error))
    }



    const doneToDo = (index) => {
        let todoListTmp = [...todoList]
        todoList[index].completed = true
        setTodoList(todoListTmp)
    }

    const selectOption = (selectedValue) => {

        setSelectDoneOrDoing(selectedValue)
    }

    return (
        <div>
            {user != undefined &&

                <div>
                    <Descriptions title={user.name}>
                        <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                        <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                        <Descriptions.Item label="Address">{user.address.street},{user.address.suite},{user.address.city},{user.address.zipcode},{user.address.street}</Descriptions.Item>
                    </Descriptions>
                </div>
            }
            <div>
                <Select value={selectDoneDoing} style={{ width: 120 }} onChange={selectOption}>
                    <Option value='all'>All</Option>
                    <Option value={false}>Doing</Option>
                    <Option value={true}>Done</Option>
                </Select>
                <List
                    header={<div>To Do List</div>}
                    footer={<div>......</div>}
                    bordered
                    dataSource={
                        selectDoneDoing == 'all' ?
                            todoList
                            :
                            todoList.filter(m => m.completed == selectDoneDoing)
                    }
                    renderItem={(item, index) => (
                        <List.Item>
                            <Typography.Text mark={!item.completed} delete={item.completed}>

                                {
                                    item.completed == false ?
                                        "DOING"
                                        :
                                        "DONE"
                                }
                                {
                                    !item.completed &&
                                    <Button style={{ float: 'right' }} onClick={() => doneToDo(index)}>DONE</Button>
                                }
                            </Typography.Text>
                            {item.title}
                        </List.Item>
                    )}

                />
            </div>
        </div>
    )
}
export default ToDoPage;


