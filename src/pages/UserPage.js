import React from 'react'
import { Input, List, Avatar, Button, Skeleton } from 'antd'
import reqwest from 'reqwest';


const { Search } = Input;
const count = 3;
const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat&noinfo`;

class UserPage extends React.Component {
    state = {
        user: []

    };

    fetchUserData() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(Response => Response.json())
            .then(data => {
                console.log(data);
                this.setState({
                    user: data
                })
            })
            .catch(error => console.log(error))
    }

    componentDidMount() {
        this.fetchUserData()
    }

    getData = callback => {
        reqwest({
            url: fakeDataUrl,
            type: 'json',
            method: 'get',
            contentType: 'application/json',
            success: res => {
                callback(res);
            },
        });
    };

    render() {

        return (
            <div>
                <Search
                    placeholder="input search text"
                    onSearch={value => console.log(value)}
                    style={{ width: 200 }}
                />
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={this.state.user}
                    renderItem={item => (
                        <List.Item
                            actions={[<a href={"/userPage/" + item.id + "/todo"}>ToDo</a>]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={item.email}
                                />
                                <div>{item.phone}/{item.website}</div>
                            </Skeleton>
                        </List.Item>
                    )}
                />
            </div>
        );
    }
}


// const UserPage = (Props) => {

//     console.log(Props);
//     return (
//         <div>
//             UserPage: {Props.match.params.name}



//         </div>
//         <div>
//             <Search
//                 placeholder="input search text"
//                 onSearch={value => console.log(value)}
//                 style={{ width: 200 }}
//             />
//             />
//         </div>
//     )

// }

export default UserPage;